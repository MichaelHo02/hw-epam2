const express = require('express');
const {
	getUserNotes,
	addNote,
	getUserNote,
	putUserNote,
	updateStatusUserNote,
	delUserNote,
} = require('../service/NoteService.js');
const { authenToken } = require('../service/AuthService.js');

const router = express.Router();
router.get('/', authenToken, getUserNotes);
router.post('/', authenToken, addNote);
router.get('/:id', authenToken, getUserNote);
router.put('/:id', authenToken, putUserNote);
router.patch('/:id', authenToken, updateStatusUserNote);
router.patch('/:id', authenToken, delUserNote);

module.exports = router;
