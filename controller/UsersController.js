const express = require('express');
const {
	getUserProfile,
	delUserProfile,
	updatePassword,
} = require('../service/UserService.js');
const { authenToken } = require('../service/AuthService.js');

const router = express.Router();
router.get('/', authenToken, getUserProfile);
router.delete('/', authenToken, delUserProfile);
router.patch('/', authenToken, updatePassword);
module.exports = router;
