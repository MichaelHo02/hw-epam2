const mongoose = require('mongoose');
const mongoosePaginate = require('mongoose-paginate-v2');

const schema = new mongoose.Schema({
	userId: {
		type: mongoose.Schema.Types.ObjectId,
		ref: 'users',
		required: true,
	},
	completed: {
		type: Boolean,
		required: true,
	},
	text: {
		type: String,
		required: true,
	},
	createdDate: {
		type: Date,
		required: true,
	},
});

schema.plugin(mongoosePaginate);

const Note = mongoose.model('notes', schema);
module.exports = Note;
