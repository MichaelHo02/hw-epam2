const Note = require('../model/Note.js');
const User = require('../model/User.js');

const getUserNotes = async (req, res) => {
	const { username } = req;
	const { offset = 0, limit = 10 } = req.query;
	if (!username) {
		return res.status(400).json({ message: 'Invalid username' });
	}
	try {
		const user = await User.findOne({ username });
		const page = await Note.paginate(
			{ userId: user._id },
			{
				offset,
				limit,
				customLabels: {
					totalPages: 'count',
					docs: 'notes',
				},
			}
		);
		return res.status(200).json({
			notes: page.notes,
			offset: page.offset,
			limit: page.limit,
			count: page.count,
		});
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
			error: error.message,
		});
	}
};

const addNote = async (req, res) => {
	const { username } = req;
	const { text } = req.body;
	if (!username || !text) {
		return res.status(400).json({ message: 'Invalid username or text' });
	}
	try {
		const user = await User.findOne({ username });
		const note = new Note({
			userId: user._id,
			completed: false,
			text,
			createdDate: new Date().toISOString(),
		});
		console.log(note);
		await note.save();
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
		});
	}
};

const getUserNote = async (req, res) => {
	const { id } = req.params;
	const { username } = req;
	if (!username || !id) {
		return res.status(400).json({ message: 'Invalid username or note id' });
	}

	try {
		const user = await User.findOne({ username });
		const note = await Note.findById(id);
		if (!user || !note) {
			return res.status(400).json({ message: 'No notes or user exists' });
		}
		return res.status(200).json({ note });
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
		});
	}
};

const putUserNote = async (req, res) => {
	const { id } = req.params;
	const { text } = req.body;
	const { username } = req;
	if (!username || !id || !text) {
		return res
			.status(400)
			.json({ message: 'Invalid username or note id or updated text' });
	}
	try {
		const user = await User.findOne({ username });
		await Note.findOneAndUpdate(
			{ _id: id, userId: user._id },
			{ $set: { text } }
		);
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
		});
	}
};

const updateStatusUserNote = async (req, res) => {
	const { id } = req.params;
	const { username } = req;
	if (!username || !id) {
		return res.status(400).json({ message: 'Invalid username or note id' });
	}
	try {
		const user = await User.findOne({ username });
		const note = await Note.findOne({ _id: id, userId: user._id });
		if (!user || !note) {
			return res.status(400).json({ message: 'No notes or user exists' });
		}
		note.completed = !note.completed;
		await note.save();
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
			error: error.message,
		});
	}
};

const delUserNote = async (req, res) => {
	const { id } = req.params;
	const { username } = req;
	if (!username || !id) {
		return res.status(400).json({ message: 'Invalid username or note id' });
	}
	try {
		const user = await User.findOne({ username });
		await Note.findOneAndRemove({ _id: id, userId: user._id });
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		res.status(500).json({
			message: 'Internal Error',
			error: error.message,
		});
	}
};

module.exports = {
	getUserNotes,
	addNote,
	getUserNote,
	putUserNote,
	updateStatusUserNote,
	delUserNote,
};
