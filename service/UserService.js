const User = require('../model/User.js');
const bcrypt = require('bcrypt');

const getUserProfile = async (req, res) => {
	const { username } = req;
	if (!username) {
		return res.status(400).json({ message: 'Bad request' });
	}
	try {
		const user = await User.findOne({ username });
		return res.status(200).json({
			user: {
				_id: user._id,
				username: user.username,
				createdDate: user.createdDate,
			},
		});
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};

const delUserProfile = async (req, res) => {
	const { username } = req;
	if (!username) {
		return res.status(400).json({ message: 'Bad request' });
	}
	try {
		const result = await User.deleteOne({ username });
		if (result.deletedCount === 0) {
			return res.status(400).json({ message: 'Cannot delete user' });
		}
		return res.status(200).json({ message: 'Success' });
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};

const updatePassword = async (req, res) => {
	const { username } = req;
	const { oldPassword, newPassword } = req.body;
	if (!username || !oldPassword || !newPassword) {
		return res.status(400).json({ message: 'Bad request' });
	}
	try {
		const user = await User.findOne({ username });
		const result = await bcrypt.compare(oldPassword, user.password);
		if (result) {
			bcrypt
				.hash(newPassword, 10)
				.then(async (hashPassword) => {
					user.password = hashPassword;
					await user.save();
					return res.status(200).json({
						message: 'Success',
					});
				})
				.catch((error) =>
					res.status(500).json({
						message: error.message,
					})
				);
		} else {
		}
	} catch (error) {
		return res.status(500).json({
			message: error.message,
		});
	}
};

module.exports = { getUserProfile, delUserProfile, updatePassword };
