const User = require('../model/User.js');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');
const dotenvExpand = require('dotenv-expand');

const myEnv = dotenv.config();
dotenvExpand.expand(myEnv);

const register = async (req, res) => {
	const { username, password } = req.body;
	if (!username || !password) {
		return res.status(400).json({
			message: 'Invalid username or password',
		});
	}

	try {
		let user = await User.findOne({ username });
		if (user) {
			return res.status(400).json({
				message: 'Username already registered',
			});
		}
		bcrypt
			.hash(password, 10)
			.then(async (hashPassword) => {
				const newUser = new User({
					username,
					password: hashPassword,
					createdDate: new Date().toISOString(),
				});
				await newUser.save();
				return res.status(200).json({
					message: 'Success',
				});
			})
			.catch(() =>
				res.status(500).json({
					message: 'Internal Error',
				})
			);
	} catch (error) {
		return res.status(500).json({
			message: 'Message',
		});
	}
};

const login = async (req, res) => {
	const { username, password } = req.body;
	if (!username || !password) {
		return res.status(400).json({
			message: 'Invalid username or password',
		});
	}
	try {
		const user = await User.findOne({ username });
		const result = await bcrypt.compare(password, user.password);
		if (result) {
			const accessToken = jwt.sign(
				username,
				process.env.ACCESS_TOKEN_SECRET
			);
			return res.status(200).json({
				message: 'Success',
				jwt_token: accessToken,
			});
		} else {
			return res
				.status(400)
				.json({ message: 'Invalid username or password' });
		}
	} catch (error) {
		return res
			.status(500)
			.json({ message: 'Internal Server Error', error: error.message });
	}
};

const authenToken = (req, res, next) => {
	try {
		const authorizationHeader = req.headers['authorization'];
		const token = authorizationHeader.split(' ')[1];
		if (!token)
			return res.send(401).json({
				message: 'No Token',
			});
		jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, data) => {
			if (err) return res.status(403).json({ error: err });
			req.username = data;
			next();
		});
	} catch (error) {
		return res
			.send(400)
			.json({ message: 'Something wrong with the token' });
	}
};

module.exports = { register, login, authenToken };
